import {Container, Row, Col, Card, Button} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import { Link, useNavigate, useParams, Navigate } from 'react-router-dom';
import Swal2 from 'sweetalert2';

export default function ProductCard(props){
   
    const {user} = useContext(UserContext);
    const [name, setProductName] = useState('');
    const {_id, productName} = props.productProp;
    const {productId} = useParams();
    const [price, setPrice] = useState('')
    const navigate = useNavigate()


    return (

        <Container className='mx-auto my-3 text-center d-block' >
            <Row className='d-block'>
                <Col className='d-block' aling='center'>
                <Card className='d-block' border="primary">
                    <Card.Body>
                        <Card.Title>{productName}</Card.Title>
                        {/* <Card.Subtitle className='mt-3'>Description</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                           
                        <Card.Title>Price</Card.Title>
                        <Card.Subtitle>Php {price}</Card.Subtitle> */}
                        
                        <Card.Header>
                        <Button className='m-1' as = {Link} to = {`/productDetails/${_id}`}>Details</Button>
                        </Card.Header>
                    </Card.Body>
                </Card>
                </Col>
            </Row>
        </Container>
    )
}