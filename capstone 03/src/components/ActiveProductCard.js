import {Container, Row, Col, Card, Button} from 'react-bootstrap'
import {useContext} from 'react';
import UserContext from '../UserContext';
import { Link,  } from 'react-router-dom';

export default function ProductCard(props){
   
    const {user} = useContext(UserContext);
  
    
    console.log(props.courseProp)
     
    const {_id, productName, description, price, isActive} = props.productProp;
    console.log(_id)

    

    return (

        <Container className='mx-auto my-3 text-center'>
            <Row>
                <Col className=''>
                <Card className=''>
                    <Card.Body>
                        <Card.Title>{productName}</Card.Title>
                        {/* <Card.Subtitle className='mt-3'>Description</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                           
                        <Card.Title>Price</Card.Title>
                        <Card.Subtitle>Php {price}</Card.Subtitle> */}
                        
                        {
                            user.isAdmin === true
                            ?
                            <Button className='m-1' as = {Link} to = {`/productDetails/${_id}`}>Details</Button>
                        
                            :

                            <Button className='m-1' as = {Link} to = {`/checkout/${_id}`}>Add to Cart</Button>
                        }

                        
                    </Card.Body>
                </Card>
                </Col>
            </Row>
        </Container>
    )
}