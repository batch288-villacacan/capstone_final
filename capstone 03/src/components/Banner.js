import {Container, Row, Col} from 'react-bootstrap';
import Carousel from 'react-bootstrap/Carousel';



export default function Banner(){


	return (
			<Container className='d-block'>
				<Row>
					<Col className='text-center col-12'>
						<Carousel>
						<Carousel.Item>
							<img
							className="d-block w-100"
							src="https://cdn.thewirecutter.com/wp-content/uploads/2019/12/webcams-lowres-2x1-0539.jpg?auto=webp&quality=75&crop=2:1&width=1024"
							alt="Web Cameras"
							/>
							<Carousel.Caption>
							<h3>Web Cameras</h3>
							</Carousel.Caption>
						</Carousel.Item>
						<Carousel.Item>
							<img
							className="d-block w-100"
							src="https://i.ytimg.com/vi/2fk8nPsZTjE/maxresdefault.jpg"
							alt="VHS Players"
							/>

							<Carousel.Caption>
							<h3>VHS Player</h3>
							</Carousel.Caption>
						</Carousel.Item>
						<Carousel.Item>
							<img
							className="d-block w-100"
							src="https://www.dexerto.com/cdn-cgi/image/width=3840,quality=75,format=auto/https://editors.dexerto.com/wp-content/uploads/2022/10/14/Best-Gaming-Keyboard.jpg"
							alt="Gaming Keyboards"
							/>

							<Carousel.Caption>
							<h3>Gaming Keyboards</h3>
							</Carousel.Caption>
						</Carousel.Item>
						</Carousel>
					</Col>
				</Row>
			</Container>
		)
}