
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {useState, useEffect} from 'react';
import {BrowserRouter, Route, Routes} from 'react-router-dom';
import {UserProvider} from './UserContext.js';
import AppNavBar from './components/AppNavbar.js'
import Home from './pages/Home.js';
import Courses from './pages/Courses.js';
import Register from './pages/Register.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import PageNotFound from './pages/PageNotFound.js';
import AdminDashboard from './pages/AdminDashboard';
import CreateProduct from './components/CreateProduct';
import Products from './pages/Products';
import UpdateProduct from './pages/UpdateProduct';
import ProductDetails from './pages/ProductDetails';
import ActiveProducts from './pages/ActiveProducts';
import ProductView from './pages/ProductView';
import Footer from './components/Footer.js'
import DashboardProduct from './pages/DashboardProduct'

function App() {

  let userDetails = {};
  useEffect(()=>{
    if(localStorage.getItem('token') === null){
      userDetails = {
        id: null,
        isAdmin: null
      }
    }else{
        fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
          method: 'GET',
          headers: {
              Authorization: `Bearer ${localStorage.getItem('token')}`
          }
      })
      .then(result => result.json())
      .then(data => {
          userDetails = {
              id : data._id,
              isAdmin: data.isAdmin
          };

      })
    }
  }, [])
  
  
  const [user, setUser] = useState(userDetails);

  const unsetUser = () => {
    localStorage.clear();
  }
  useEffect(()=> {
    console.log(user);
    console.log(localStorage.getItem('token'));
  }, [user])


  

  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
        <BrowserRouter>
          <AppNavBar />
          <Footer />

           { 
          //  Logged Out
            localStorage.getItem('token') === null

            ?
            
            <Routes>
              <Route path = '/' element = {<Home/>} />
              <Route path = '/allProducts' element = {<Products/>} />
              <Route path = '/register' element = {<Register/>} />
              <Route path = '/login' element = {<Login/>} />
            </Routes>
            : 
            // Logged In

            <Routes>
              <Route path = '/logout' element = {<Logout/>} />
              <Route path = '/' element = {<Home/>} />
              <Route path = '/allProducts' element = {<Products/>} />
              <Route path = '/dashboard' element = {<AdminDashboard/>} />
              <Route path = '/dashboard2' element = {<DashboardProduct/>} />
              <Route path = '/createProduct' element = {<CreateProduct/>} />
              <Route path = '/updateProduct/:productId' element = {<UpdateProduct/>} />
              <Route path = '/productDetails/:productId' element = {<ProductDetails/>} />
              <Route path = '/:productId/productStatus' element = {<ProductDetails/>} />
              <Route path = '*' element = {<PageNotFound/>} />
              <Route path = '/activeProducts' element = {<ActiveProducts />} />
              <Route path = '/checkout/:productId' element = {<ProductView />} />
              <Route path = '/adminDashboard/' element = {<ProductView />} />
            </Routes>
            }

        </BrowserRouter>
    </UserProvider>
    

  );
}

export default App;
