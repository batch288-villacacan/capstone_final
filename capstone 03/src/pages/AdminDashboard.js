	import {Container, Row, Col, Card, Button} from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';
import CreateProduct from '../components/CreateProduct';
import RetrieveAllProducts from '../components/RetrieveAllProducts';
import { Link, Navigate } from 'react-router-dom';
import {useContext} from 'react';
import UserContext from '../UserContext';

export default function AdminDashboard(){
	
	const {user,setUser} = useContext(UserContext);
	
	return(
		  
		user.isAdmin === true
		
		?
		<Container>
			<h1 className='text-center'>Dashboard</h1>
		<Row>
			<Col className="d-block" align="center">
				{/* <h2 className="text-center mt-3">Create Product</h2> */}
				<Card className="text-center col-sm-12 mt-4 cardHeight">
				<Card.Body>
					<Card.Title>Create Product</Card.Title>
					<Card.Text>
					Show your new product to the world! Add your new product to the listing.
					</Card.Text>
					<Button variant="primary" as = {Link} to = '/createProduct'>Add product</Button>
				</Card.Body>
				</Card>
			</Col>

			<Col className="d-block" align="center">
                    {/* <h2 className="text-center mt-3">Retrieve All Products</h2> */}
                    <Card className="text-center col-sm-12 mt-4 cardHeight">
                    <Card.Body>
                        <Card.Title>Retrieve All Products</Card.Title>
                        <Card.Text>
                        Show all your listed active and inactive products.
                        </Card.Text>
                        <Button className="m-1" variant="primary" as = {Link} to = '/allProducts'>Product Management</Button>

						<Button className="m-1" variant="primary" as = {Link} to = '/dashboard2'>Product Handling</Button>
                    </Card.Body>
                    </Card>
                </Col>
		</Row>
		</Container>
		:
		<Navigate to = '/PageNotFound' />
		
		
			
			
			
		
)
}