import { Container, Row, Col, Card, Button, Form, FloatingLabel } from "react-bootstrap";
import { useState, useEffect, useContext} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Swal2 from 'sweetalert2';
import UserContext from '../UserContext.js';
import {useNavigate, useParams} from 'react-router-dom';



export default function UpdateProduct(){

    const [productName, setProductName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');
    const {productId} = useParams();
    const {user, setUser} = useContext(UserContext);

    const navigate = useNavigate();
    const [isDisabled, setIsDisabled] = useState(true);

    useEffect(()=>{
		if (productName !== '' || description !== '' || price !== ''){

			setIsDisabled(false);

		}else{
			setIsDisabled(false);
		}
    }, []);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
            .then(result => result.json())
            .then(data => {
                console.log(data);
                setProductName(data.productName);
                setDescription(data.description);
                setPrice(data.price)
            })
    }, [])

    function updateToProduct(event){
        event.preventDefault()

        if (user.isAdmin === true){

        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
            method: 'PATCH',
            headers: {
                'Content-Type' : 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                productName : productName,
                description : description,
                price : price
            })
        })
        .then (result => result.json())
        .then (data => {
            if (data === false){
                Swal2.fire({

                    title: 'Failed to update product',
                    icon: 'error',
                    text: 'Try again!'

                })
            } else {
                Swal2.fire({
                    title: 'Product updated successfully!',
                    icon: 'success',
                })

                navigate(`/productDetails/${productId}`)
            }
        })

    } else {
        navigate('/PageNotFound')
    }

    }


    return ( <>
        <Container>
            <Row>
                <h1 className="text-center"> Update Product</h1>
                <Col>
                    <Form onSubmit={event => updateToProduct(event)}>
                        <Form.Group>
                        <FloatingLabel
                            controlId="floatingInput"
                            label="Product Name"
                            className="mb-3"
                        >
                            <Form.Control type="text" placeholder="Product Name"
                            value = {productName} 
                            onChange = {event => {
                                setProductName(event.target.value)
                            }}
                            />
                        </FloatingLabel>
                        
                        <FloatingLabel controlId="floatingInput" label="Description" className="mb-3">
                            <Form.Control type="textarea" placeholder="Description" value = {description} 
                            onChange = {event => {
                                setDescription(event.target.value)
                            }}
                            />
                        </FloatingLabel>

                        <FloatingLabel controlId="floatingInput" label="Price" className="mb-3" >
                            <Form.Control type="number"
                             placeholder="Price" value={price}
                             onChange = {event => {
                                setPrice(parseFloat(event.target.value))
                            }}
                            />
                        </FloatingLabel>
                        </Form.Group>

                        <Form.Group className="d-grid gap-2">
                        <Button variant="primary" size="lg" type = "submit" disabled = {isDisabled}>
                            Update Product
                        </Button>
                        </Form.Group>
                        
                    </Form>
                </Col>
            </Row>
        </Container>
    </>)
}