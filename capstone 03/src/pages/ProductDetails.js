import {useState, useEffect, useContext} from 'react';
import{useParams, useNavigate, Navigate, Link} from 'react-router-dom';
import Swal2 from 'sweetalert2';
import {Container, Row, Col, Button, Card} from 'react-bootstrap';
import UserContext from '../UserContext';


export default function ProductDetails(){
    
    const {productId} = useParams();
    const {user} = useContext(UserContext);
    const navigate = useNavigate();
	
    const [productName, setProductName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
    const [active, setIsActive] = useState('');

    console.log(productId);

	useEffect(() => {
		
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
            method: 'GET',
            headers: {
                'Content-Type' : 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
		.then(result => result.json())
		.then(data => {
			setProductName(data.productName);
			setDescription(data.description);
			setPrice(data.price);
            setIsActive(data.isActive);
            console.log(data);
		})

	}, [])

    useEffect(() => {
		
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(result => result.json())
		.then(data => {
			setProductName(data.productName);
			setDescription(data.description);
			setPrice(data.price);
            setIsActive(data.isActive);
		})

	}, [])

    const changeStatus = (e) => {
        e.preventDefault()
   
        if (user.isAdmin === true){

            fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/productStatus`, {
                method: 'PATCH',
                headers: {
                    'Content-Type' : 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify({
                    isActive : active
                })
            })
            .then (result => result.json())
            .then (data => {
                if (data === false){
                    Swal2.fire({
                        title: 'Product is now back on shelf!',
                        icon: 'success',
                        text: 'product activated!'
                    })
                } else {
                    Swal2.fire({

                        title: 'Good riddance! Product is now archived!',
                        icon: 'info',
                        text: 'product archived!'

                    })
                
                    navigate(`/productDetails/${productId}`)
                }
            })

        } else {
            navigate('/PageNotFound')
        }

    }

    ////////////////////////////////

    // const addtocart = (productId) => {

	// 	if (user.isAdmin !== true){
	// 		fetch(`${process.env.REACT_APP_API_URL}/orders/addtocart`, {
	// 			method: 'POST',
	// 			headers: {
	// 				'Content-Type' : 'application/json',
	// 				'Authorization': `Bearer ${localStorage.getItem('token')}`
	// 			}, 
	// 			body: JSON.stringify({
	// 				id : productId,
	// 			})
	// 		})
	// 		.then (response => response.json())
    //         .then (data => {
    //         if (data){
    //             Swal2.fire({
    //                 title: `product added to cart!`,
    //                 icon: 'success',
    //             })
                
    //             navigate(`/allProducts`)
    //         } else {
    //             Swal2.fire({

    //                 title: 'Failed to add product',
    //                 icon: 'error',
    //                 text: 'Try again!'

    //             })
    //         }
    //     })

	// 	} else {
    //         Swal2.fire({

    //             title: 'Non admin ONLY',
    //             icon: 'error',
    //             text: 'Log in to your user account'
    //         })
    //     	navigate('/PageNotFound')
    // 	}
	// }


return(
	<Container>
		<Row>
			<Col>
				<Card> 
				      <Card.Body>
				        <Card.Title>{productName}</Card.Title>
				        
				        <Card.Subtitle>Description:</Card.Subtitle>
				        <Card.Text>{description}</Card.Text>

				        <Card.Subtitle>Price:</Card.Subtitle>
				        <Card.Text>P {price}</Card.Text>

                        {
                            user.isAdmin === true 
                            ?
                            <Button className="m-1" variant="primary" as = {Link} to = {`/updateProduct/${productId}`}>Update</Button>
                            :
                            <></>
                        }

                        

                       {
                            active && user.isAdmin === true
                           ?
                           <Button className="m-1" variant="primary" onClick={event => changeStatus(event)}>Archive</Button>
                           :
                           <></>
                           
                       }

                        
                        {
                            !active && user.isAdmin === true
                           ?
                            <Button className="m-1" variant="primary" onClick={event => changeStatus(event)}>Activate</Button>
                            :
                            <></>
                            
                       }
                        


                       {
                            user.isAdmin === true
                            ?
                            <></>
                            :
                            <Button className='m-1' as = {Link} to = {`/checkout/${productId}`}>Add to Cart</Button>
                       }
                      

                     

				      </Card.Body>
				</Card>
			</Col>
		</Row>
	</Container>
	)
}