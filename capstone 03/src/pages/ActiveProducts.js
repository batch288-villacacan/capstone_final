import ActiveProductCard from '../components/ActiveProductCard'
import { useState, useEffect } from 'react';

export default function Products(){
	
	const [activeProducts, setProducts] = useState([]);
	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/products/activeProducts`)
		.then(result => result.json())
		.then(data => {
			// console.log(data)
			setProducts(data.map(product => {
				return(
					<ActiveProductCard key = {product._id} productProp = {product}/>
				)
			}))
		})
	}, [])

	return(
		<>
		
		<h1 className='mt-3 text-center'>Active Products</h1>
		
		{activeProducts}
		
		</>
		
	)
	
}