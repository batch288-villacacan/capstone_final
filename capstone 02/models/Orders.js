// SESSION 42 - DATA MODEL DESIGN

const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	
	userId: {
		type: String,
		required: [true, "userId is required"]
	},

	products: [{
		productId: {
			type: String,
			required: [true, "missing productId"]
		},

		quantity: {
			type: Number,
			default: 0
		},

		totalAmount: {
			type: Number,
			default: 0
		},

		purchasedOn: {
			type: String,
			default: new Date()

		}
	}]
}) //END MODEL

const Orders = mongoose.model("Order", orderSchema);
module.exports = Orders;