const jwt = require("jsonwebtoken");
const secret = "@ec583";

// CREATING JSON WEB TOKEN
module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		isAdmin: user.isAdmin,
		email: user.email
	}
	return jwt.sign(data, secret, {});
}

// TOKEN VERIFICATION
module.exports.verify = (request,response, next) => {
	let token = request.headers.authorization;

	if(token !== undefined){
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (error, data) =>{
			if(error){
				return response.send("Authorization failed!")
			} else {
				next();
			}
		})
		} else {
			return response.send ("No token provided");
		}
	}

// TOKEN DECODING
module.exports.decode = (token) => {
	token = token.slice(7, token.length);
	return jwt.decode(token, {complete: true})
	.payload;
}