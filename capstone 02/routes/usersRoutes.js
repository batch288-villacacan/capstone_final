const express = require('express');
const usersControllers = require('../controllers/usersControllers.js')
const productsControllers = require('../controllers/productsControllers.js')
const router = express.Router();
const auth = require("../auth.js")

// REGISTRATION ROUTE
router.post("/register", usersControllers.registerUser);

// LOGIN ROUTE
router.post("/login", usersControllers.loginUser);

// RETRIEVE USER DETAILS
router.get("/details", usersControllers.userDetails)

// GET ALL USERS (ADMIN ONLY)
router.get("/all", auth.verify, usersControllers.getAllUsers)

// S45 RETRIEVE USER DETAILS (CURRENTLY LOGGED IN)
router.get("/profile", auth.verify, usersControllers.myProfile)

// USER DETAILS
router.get("/userDetails", auth.verify, usersControllers.retrieveUserDetails);

// SET A USER AS ADMIN (ADMIN ONLY)
router.patch("/:userId/admin", auth.verify, usersControllers.setAsAdmin)

// SET A USER AS ADMIN (ADMIN ONLY)
router.patch("/:userId/dgAdmin", auth.verify, usersControllers.setAsUser)

// CREATE ORDER
router.post("/checkout", usersControllers.checkout)
module.exports = router;