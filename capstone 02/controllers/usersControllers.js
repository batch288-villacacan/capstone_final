const Users = require("../models/Users.js");
const Products = require("../models/Products.js");
const bcrypt = require("bcrypt");
const auth = require ("../auth.js");

// REGISTRATION OF A USER

module.exports.registerUser = (request, response) => {
	Users.findOne ({email: request.body.email})
	.then(result => {

		if(result){
			return response.send(false)
		} else {
			let newUser = new Users ({
				email: request.body.email,
				password: bcrypt.hashSync(request.body.password, 9),
				isAdmin: request.body.isAdmin,
			})

			newUser.save()
			.then(saved => response.send (true))
			.catch(error => response.send(error));
		}
	})
	.catch(error => response.send(error));
}

// AUTHENTICATION OF THE USER

module.exports.loginUser = (request, response) => {
    Users.findOne({email: request.body.email})
    .then(result => {
        if (!result){
            return response.send(false)
        } else {
         
            const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

            if(isPasswordCorrect){
                return response.send({
                auth: auth.createAccessToken(result)
            })
            } else {
                return response.send(false)
            }
        }
    })
    .catch(error => response.send(false));
}
// RETRIEVING USER DETAILS

module.exports.userDetails = (request, response) => {

    const userData = auth.decode(request.headers.authorization)

    // console.log(userData)

    if(userData.isAdmin){
            Users.findById(request.body.id)
            .then(result => {
                result.password ="";
                return response.send(result)
            })

            .catch(error => {
                return response.send(error);
            })
    } else {
            return response.send(false)
    }
}

// RETRIEVE USER DETAILS
module.exports.retrieveUserDetails = (request, response) => {
    const userData = auth.decode(request.headers.authorization);

    Users.findOne({_id : userData.id})
    // .then(result => result.toJSON())
    .then(data => response.send(data))
}

// SET A USER TO ADMIN
module.exports.setAsAdmin = (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	const userId = request.params.userId

	let nowAdmin = {
		isAdmin: true
	}

	if(userData.isAdmin){
		Users.findById(request.params.userId)
		.then(result =>{
			if(result.isAdmin === false){
				Users.findByIdAndUpdate(userId,nowAdmin)
				.then(result => response.send("congrats, user is now an admin"))
				.catch(error => response.send(error))
			} else {
				return response.send("User is already an admin")
			}
		})
		.catch(error => response.send(error))
	} else {
		return response.send("You are not allowed to change user roles.")
	}
}

// SET A ADMIN TO USER
module.exports.setAsUser = (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	const userId = request.params.userId

	let nowAdmin = {
		isAdmin: false
	}

	if(userData.isAdmin){
		Users.findById(request.params.userId)
		.then(result =>{
			if(result.isAdmin){
				Users.findByIdAndUpdate(userId,nowAdmin)
				.then(result => response.send("Admin privileges has been removed to the user"))
				.catch(error => response.send(error))
			} else {
				return response.send("User is not an admin")
			}
		})
		.catch(error => response.send(error))
	} else {
		return response.send("You are not allowed to change user roles.")
	}
}

// SHOW ALL USERS (ADMIN ONLY)
module.exports.getAllUsers = (request, response) => {
	const userData = auth.decode(request.headers.authorization)

	if(userData.isAdmin){
		Users.find({})
		.then(result => response.send(result))
		.catch(error => response.send(error))
	} else {
		return response.send("oopsie")
	}
	
}

// USER CHECKOUT
module.exports.checkout = (request, response) => {
	const userId = request.body.id;
	const userData = auth.decode(request.headers.authorization)

	if(!userData.isAdmin){
		return response.send(true)
	} else {
		let productUpdated = Products.findOne({_id : userData})
	}
}

module.exports.myProfile = (request, response) => {
	const userData = auth.decode(request.headers.authorization)

	Users.findById(userData.id)
		.then(result => {
			return response.send(result)
		})
		.catch (error => {
			return response.send(error)
		})
	
}

module.exports.retrieveUserDetails = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	Users.findOne({_id: userData.id})
	.then(data => response.send(data));
}