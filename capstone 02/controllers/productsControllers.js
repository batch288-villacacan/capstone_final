const Users = require("../models/Users.js");
const Products = require("../models/Products.js");
const bcrypt = require("bcrypt");
const auth = require ("../auth.js");


// CREATING/ADDING A PRODUCT (ADMIN ONLY)

module.exports.addProduct = (request, response) => {


	const userData = auth.decode(request.headers.authorization)

	if(userData.isAdmin){
	
		let newProduct = new Products({
			productName: request.body.productName,
			description: request.body.description,
			price: request.body.price
		})


		newProduct.save()
		.then(save => response.send(true))
		.catch(error => response.send(error))
	} else {
		return response.send(false)
	}

}

// RETRIEVING PRODUCTS
module.exports.getAllProducts = (request, response) => {
	Products.find({})
	.then(result => response.send(result))
	.catch(error => response.send (error));
}

// RETRIEVE ACTIVE PRODUCTS
module.exports.getActiveProducts = (request, response) => {
	Products.find({isActive:true})
	.then(result => response.send(result))
	.catch(error=> response.send(error));
}

// RETRIEVE ACTIVE PRODUCTS
module.exports.getInactiveProducts = (request, response) => {
	Products.find({isActive:false})
	.then(result => response.send(result))
	.catch(error=> response.send(error));
}

// RETRIEVE A SINGLE PRODUCT
module.exports.getProduct = (request, response) => {
	const productId = request.params.productId;
	Products.findById(productId)
	.then(result => response.send(result))
	.catch(error => response.send(error));
}

// UDPATE A PRODUCT INFORMATION (ADMIN ONLY)
module.exports.updateProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const productId = request.params.productId;
	
	let updatedProduct = {
		productName: request.body.productName,
		description: request.body.description,
		price: request.body.price
	}

	if(userData.isAdmin){
		Products.findByIdAndUpdate(productId, updatedProduct)
		.then(result => response.send(true))
		.catch(error => response.send(error))
	} else {
		return response.send(false)
	}
}


// SESSION 44 - ARCHIVE/ACTIVATE A PRODUCT (ADMIN ONLY)
module.exports.productStatus = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const productId = request.params.productId;

	let archiveProduct = {isActive: false}
	let activateProduct = {isActive: true}

	if(userData.isAdmin){
		Products.findById(productId)
		.then(result => {
			if(result.isActive){
				Products.findByIdAndUpdate(productId, archiveProduct)
				.then (result => response.send(true))
				.catch (error => response.send(false))
			} else {
				Products.findByIdAndUpdate(productId, activateProduct)
				.then (result => response.send(false))
				.catch (error => response.send(false))
			}
		}).catch (error => response.send(false))

	} else {
		return response.send(false)
	}

}


// ARCHIVE A PRODUCT (ADMIN ONLY)
module.exports.archiveProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	const productId = request.params.productId
	
	let archivedProduct = {
		isActive: false
	}

	if(userData.isAdmin){
		Products.findByIdAndUpdate(productId, archivedProduct)
		.then (result => response.send(true))
		.catch (error => response.send(error))
	} else {
		return response.send(false)
	}
}

// ACTIVATE A PRODUCT (ADMIN ONLY)
module.exports.activateProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const productId = request.params.productId;
	let activatedProduct = {	
		isActive: true
	}

	if(userData.isAdmin){
		Products.findByIdAndUpdate(productId, activatedProduct)
		.then (result => response.send(true))
		.catch (error => response.send(error))
	} else {
		return response.send(false)
	}
}
