const Users = require("../models/Users.js");
const Products = require("../models/Products.js");
const Orders = require("../models/Orders.js");
const bcrypt = require("bcrypt");
const auth = require ("../auth.js");

// CREATE ORDER / CHECKOUT
module.exports.addToCart = (request, response) => {

	const productId = request.body.productId
    userData = auth.decode(request.headers.authorization)
	console.log(userData.id)


    if(userData.isAdmin){
    	return response.send(false)
    } else {
		let newOrder = new Orders({
			userId: userData.id,
			products: [{
					productId: request.body.productId,
					quantity: request.body.quantity,
					totalAmount: request.body.totalAmount
				}]
		})

		newOrder.save()
		.then(saved => response.send(true))
		.catch(error => response.send(false));

    	// Orders.findOne({userId: userData.id})
    	// .then(result => {
		// 	console.log(result)
    	// if(result){
    	// 	Orders.findOne({userId: userData.id})
    	// 	.then(result=> {
    	// 		result.products.push({
    	// 			productId: request.body.productId,
		//     		quantity: request.body.quantity,
		//     		totalAmount: request.body.totalAmount
		//     	})
    	// 		return response.send(true)
    	// 		result.save()
        //    		.then(saved => true)
        //    		.catch(error => false)
    	// 	}).catch(error => false)

    	// } else {
    	// 	let newOrder = new Orders({
    	// 		userId: userData.id,
    	// 		products: [{
		//     			productId: request.body.productId,
		//     			quantity: request.body.quantity,
		//     			totalAmount: request.body.totalAmount
		//     		}]
    	// 	})

    	// 	newOrder.save()
    	// 	.then(saved => response.send(true))
        //     .catch(error => response.send(false));

    // 	}
    // }).catch(error => response.send(false));
    }


}

// module.exports.addToCart = (request, response) => {
// 	const productId = request.body.id;
// 	const userData = auth.decode(request.headers.authorization);

// 	if(userData.isAdmin){
// 		return response.send(false);
// 	} else {
// 		let isUserUpdated = Users.findOne({_id : userData.id})
// 		.then(result => {
// 			result.orderedProduct.products.push({
// 				productId : productId
// 			})

// 			result.save()
// 			.then(saved => true)
// 			.catch(error => false)
// 		})
// 		.catch (error => false)

// 		let isProductUpdated = Products.findOne({_id : productId})
// 		.then (result => { result.userOrders.userId.push({
// 			userId : userData.id

// 		});
// 		result.save()
// 		.then(saved => true)
// 		.catch(error => false)
		
// 		})
// 		.catch(error => false)
// 		if (isUserUpdated && isProductUpdated){
// 			return response.send(true);
// 		} else {
// 			return response.send(false)
// 		}
// 	}
// }

// RETRIEVE ALL ORDERS (ADMIN ONLY)

module.exports.getAllOrders = (request, response) => {
	const userData = auth.decode(request.headers.authorization)

	if(userData.isAdmin){
		Orders.find({})
		.then(result => response.send(result))
		.catch(error => response.send(error))
	} else {
		return response.send("For administrator's eyes only")
	}
}

// CART

// RETRIEVE AUTHENTICATED USER'S ORDER
module.exports.viewCart = (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	
	if(userData.id)
	Orders.find({ userId : userData.id })
	.then(result => response.send(result))
	.catch(error => response.send(error))
}


// CHANGE PRODUCT QUANTITIES
module.exports.updateProductQuantity = (request, response) =>{
	const userData = auth.decode(request.headers.authorization)
	
	let productId = {productId: request.body.productId}
	let updateQuantity = {quantity: request.body.quantity}
	
	if(userData.id){
	Orders.findOne({ userId : userData.id })
	.then(result => {
		Orders.findByIdAndUpdate(productId, updateQuantity)
		.then(result => response.send(`Product quantity changed to ${updateQuantity}`))
		.catch(error => response.send(error))
	})

	.catch(error => response.send(error))
	} else {
		return response.send("login to your account")
	}

}
